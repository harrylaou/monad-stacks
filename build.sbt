name := "monad-stacks"
version := "1.0"
scalaVersion := "2.11.11"

lazy val EmmVersion =  "0.2.1"


libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play" % "2.5.15",
  "org.typelevel" %% "cats" % "0.9.0",

  "com.codecommit" %% "emm-core" % EmmVersion,
  "com.codecommit" %% "emm-cats" % EmmVersion
)


resolvers += Resolver.sonatypeRepo("releases")

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.4")


//This is needed for free monads ,
//but when enabled emm example fails

scalacOptions += "-Ypartial-unification"
