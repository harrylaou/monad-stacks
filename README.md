# monad-stacks



This the code repository for my presentation 

Videos :

- [Typelevel summit 2017 , Copenhagen](https://www.youtube.com/watch?v=2TDDDFGa8-0)
- [Swarm conference 2017, Porto](https://www.youtube.com/watch?v=sSHCbWkaE54) 
- [Scala Central Meetup , London](https://www.youtube.com/watch?v=qWfK18VQ3FI)
- [Scala UA 2018 , Kiev](https://www.youtube.com/watch?v=BVqQu0j-REw)


Slides : 

- [Monad Stacks or: How I Learned to Stop Worrying and Love the Free Monad](http://www.harrylaou.com/slides/MonadStacks.pdf)



### Note : SI-2712-fix plugin

```scala
scalacOptions += "-Ypartial-unification"
```

- if not enabled, free monad code doesn't compile !
- if enabled emm code doesn't compile !