package com.harrylaou.monadstacks.emm

import _root_.emm._
import _root_.emm.compat.cats._
import com.harrylaou.monadstacks.commons.Functions._
import com.harrylaou.monadstacks.commons.{Erratum, User}
import play.api.libs.json.Reads
import User._
import play.api.Logger.logger
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}
import cats.implicits._
import play.api.libs.json.Json

/**
  *
  * This code demonstrates the practicality and usefulness of advanced functional programming techniques
  * like generalized effect composition libraries, in this example Emm. https://github.com/djspiewak/emm
  * It also proves by example and comparison that even is simple operations like parsing a json
  * and updating a model in db, the usage of Emm produces much clearer and concrete code.
  *
  * We need a definition of the monad stack
  * type FXE = Future |: Either[Erratum, ?] |: Base
  *
  * and use a for-comprehension by lifting the values in the effect stack using :
  *
  * pointM[FXE] – in the case of a Base type , like Unit or User
  * liftM[FXE] – in the case of a  Either[Erratum, Base]
  * wrapM[FXE] – Future[Either[Erratum, Base]]
  *
  * You can compare how much more complex the code is when not using Emm ,
  * but with map, flatMap and embedded pattern matching.
  *
  * Without using Emm the function is 26-lines of code long
  * and much more complex because of the embedded pattern matching
  *
  * Using Emm, the code becomes very clean and 8-lines long.
  *
  *
  */
class UserController(implicit ec: ExecutionContext) extends Controller {

//  implicit val userFormat:Reads[User] = Json.reads[User]
  /**
    * Definition of the monad stack
    */
  type MonadStack = Future |: Either[Erratum, ?] |: Base

//  def saveUser: Action[AnyContent] = Action.async { implicit request =>
//
//
//    val savedUserE: Emm[MonadStack, User] = for {
//      jsValue   <- parseRequest(request).liftM
//      user      <- fromJson(jsValue).liftM
//      savedUser <- saveE(user).wrapM
//      _         <- logger.debug(s"User $user updated").pointM
//    } yield savedUser
//
//    stackToResult[User](savedUserE.run)
//  }

}
