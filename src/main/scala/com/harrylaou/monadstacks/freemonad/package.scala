package com.harrylaou.monadstacks


import cats.data.EitherT
import cats.free.Free
import cats.~>
import cats.implicits._
import com.harrylaou.monadstacks.commons.Erratum
import com.harrylaou.monadstacks.freemonad.Algebra._
import com.harrylaou.monadstacks.monadtransformer.EitherTUtils._
import scala.concurrent.Future
/**
  * Write an interpreter for your program
  */
package object freemonad {


  type Prg[A] = Free[Alg, A]


  def interpreter: Alg ~> EitherT[Future, Erratum, ?] = new (Alg ~> EitherT[Future, Erratum, ?]) {
    override def apply[A](alg: Alg[A]): EitherT[Future, Erratum, A] =
      alg match {
        case fb@FromBase(a)        => a.pureET(fb.ap)
        case fe@FromEither(either) => either.liftET(fe.ap)
        case ff@FromFut(fut)     => fut.liftET(ff.ec)
        case FromFutEither(fet) => fet.wrapET
      }
  }
}
