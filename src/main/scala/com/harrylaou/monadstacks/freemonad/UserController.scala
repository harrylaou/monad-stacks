package com.harrylaou.monadstacks.freemonad


import cats.implicits._
import com.harrylaou.monadstacks.commons.Functions.{fromJson, parseRequest, saveE, stackToResult}
import com.harrylaou.monadstacks.commons.{Erratum, User}
import User._
import play.api.Logger.logger
import play.api.libs.json.{Format, Json, OFormat, Reads}
import play.api.mvc.{Action, AnyContent, Controller}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}
import FMUtils._
import cats.data.EitherT
/**
  *
  */
class UserController(implicit ec: ExecutionContext) extends Controller {


  def  saveUser: Action[AnyContent] = Action.async { implicit request =>
    val prg: Prg[User]  = for {
      jsValue   <- parseRequest(request).liftFM
      user      <- fromJson(jsValue).liftFM
      savedUser <- saveE(user).wrapFM
      _         <- logger.debug(s"User $user updated").pureFM
    } yield savedUser


    val savedUser:EitherT[Future, Erratum, User] = prg.foldMap(interpreter)

    stackToResult[User](savedUser.value)
  }

}
