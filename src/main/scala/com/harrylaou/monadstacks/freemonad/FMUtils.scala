package com.harrylaou.monadstacks.freemonad
import Algebra._
import cats.Applicative
import cats.free.Free
import com.harrylaou.monadstacks.commons.Erratum
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ExecutionContext, Future}

/**
  *Create smart constructors using extension methods
  */
object FMUtils {
  implicit class FMFromA[A](a: A) {
    def pureFM(implicit ap: Applicative[Future]): Free[Alg, A] = Free.liftF(FromBase(a))
  }

  implicit class FMFromEither[A](either: Either[Erratum, A]) {
    def liftFM(implicit ap: Applicative[Future]): Free[Alg, A] =
      Free.liftF(FromEither(either))
  }
  implicit class FMFromFuture[A](fut: Future[A]) {
    def liftFM(implicit ec: ExecutionContext): Free[Alg, A] = Free.liftF(FromFut(fut))
  }

  implicit class FMFromFE[A](fet: Future[Either[Erratum, A]]) {
    def wrapFM: Free[Alg, A] =
      Free.liftF(FromFutEither(fet))
  }
}
