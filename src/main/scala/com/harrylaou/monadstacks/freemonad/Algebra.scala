package com.harrylaou.monadstacks.freemonad

import cats.Applicative
import com.harrylaou.monadstacks.commons.Erratum

import scala.concurrent.{ExecutionContext, Future}

/**
  * Create an ADT representing your grammar
  */
object Algebra {
  sealed trait Alg[A]

  type FET[B] = Future[Either[Erratum, B]]

  case class FromBase[A](a: A)(implicit val  ap: Applicative[Future]) extends Alg[A]

  case class FromEither[A](either: Either[Erratum, A])(implicit val ap: Applicative[Future]) extends Alg[A]

  case class FromFut[A](fut: Future[A])(implicit val ec: ExecutionContext) extends Alg[A]

  case class FromFutEither[A](fut: Future[Either[Erratum, A]]) extends Alg[A]

}
