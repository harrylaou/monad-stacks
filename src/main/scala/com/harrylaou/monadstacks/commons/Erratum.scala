package com.harrylaou.monadstacks.commons

import play.api.mvc.{Result, Results}

/**
  * Represents an error.
  *
  * The name was chosen so as not to class with
  * a bazillion other `Error` classes in other libraries.
  * So here the Error (Erratum) is the product of status , which its type is a Play type,
  * but can be anything else that represents an HTTP status code
  * and a simple String message
  *
  */
case class Erratum(status: Results.Status, message: String) {
  def toResult: Result =
    status(message)
}

object Erratum {

  def notFound(msg: String)         = Erratum(Results.NotFound, msg)
  def methodNotAllowed(msg: String) = Erratum(Results.MethodNotAllowed, msg)
  def withMessage(msg: String)      = Erratum(Results.InternalServerError, msg)
  def from(th: Throwable)           = Erratum(Results.InternalServerError, th.getMessage)
  //def from(we: Whatever)          = Erratum(Results.XXX, we.yyy)

}
