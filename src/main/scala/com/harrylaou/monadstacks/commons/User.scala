package com.harrylaou.monadstacks.commons

import play.api.libs.json.{Format, Json, OFormat, Reads}

case class User(email: String)


object User {
 implicit val userFormat: Format[User] = Json.format[User]
}
