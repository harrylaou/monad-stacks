package com.harrylaou.monadstacks.commons

import cats.implicits._
//import emm._
//import emm.compat.cats._
import play.api.Logger.logger
import play.api.libs.json.{Format, JsValue, Json, Reads, Writes}
import play.api.mvc.Results.Ok
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

/**
  * This is just a holder of functions and implicits.
  * These can be anywhere in the codebase but for demo reasons
  * they are in this example.
  */
object Functions {

  /**
    * Generic (polymorphic) function that parses a JsValue and returns the disjunction of an error and a value of A
    *
    * @param jsValue
    * @param reads implicit parameter of a play.api.libs.json.Reads[A]
    * @tparam A
    * @return
    */
  def fromJson[A](jsValue: JsValue)(implicit reads: Reads[A]): Either[Erratum, A] =
    Json
      .fromJson[A](jsValue)(reads)
      .asEither
      .leftMap(e => Erratum(status = Results.UnprocessableEntity, message = e.toString()))

  /**
    * helper function to parse a request
    *
    * @param request
    * @return Xor[Erratum, JsValue]
    */
  def parseRequest(request: Request[AnyContent]): Either[Erratum, JsValue] = {
    val jsonOpt: Option[JsValue] = request.body.asJson
    jsonOpt.toRight(
      Erratum(status = Results.UnprocessableEntity,
              message = s"cannot parse json for ${request.body}"))
  }

  /**
    * Could be slick or mongo
    */
  def save(u: User)(implicit ec: ExecutionContext): Future[Option[User]] =
    Future.successful(Some(User("harrylaou@gmail.com")))

  /**
    * we want more semantics in error
    */
  def saveE(u: User)(implicit ec: ExecutionContext): Future[Either[Erratum, User]] =
    save(u).map(
      Either.fromOption(_, Erratum(Results.UnprocessableEntity, s"cannot save user :$u")))

  /**
    *
    * Polymorphic function that transforms the stacked monad into an synchronous result.
    *
    * @param fxo      the stacked monad
    * @param jsWrites the implicit play.api.libs.json.Writes
    * @param ec       implicit ExecutionContext
    * @tparam A type parameter of a model
    * @return
    */
  def stackToResult[A](fxo: Future[Either[Erratum, A]])(implicit jsWrites: Writes[A], ec: ExecutionContext): Future[Result] =
    fxo.map {
      case Left(erratum) =>
        logger.error(erratum.toString)
        erratum.toResult
      case Right(user) => Ok(Json.toJson(user))
    }

}
