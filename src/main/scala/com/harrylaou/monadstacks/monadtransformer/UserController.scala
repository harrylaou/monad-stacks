package com.harrylaou.monadstacks.monadtransformer

import cats.implicits._
import EitherTUtils._
import com.harrylaou.monadstacks.commons.Functions._
import com.harrylaou.monadstacks.commons.User
import play.api.Logger.logger
import play.api.libs.json.{Json, Reads}
import play.api.mvc._
import scala.concurrent.ExecutionContext

class UserController(implicit ec: ExecutionContext) extends Controller {

  implicit val reads: Reads[User] = Json.format[User]

  def saveUser = Action.async { implicit request =>
    val savedUserE = for {
      jsValue   <- parseRequest(request).liftET
      user      <- fromJson(jsValue).liftET
      savedUser <- saveE(user).wrapET
      _         <- logger.debug(s"User $user updated").pureET
    } yield savedUser

    stackToResult[User](savedUserE.value)
  }

}
