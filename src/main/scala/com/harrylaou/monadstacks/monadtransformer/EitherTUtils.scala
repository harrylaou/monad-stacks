package com.harrylaou.monadstacks.monadtransformer

import cats.Applicative
import cats.data.EitherT
import cats.implicits._
import com.harrylaou.monadstacks.commons.Erratum

import scala.concurrent.{ExecutionContext, Future}

/**
  *These are some implicit classes to lift several types to the monad transformer with nicer syntax
  *
  */
object EitherTUtils {

  implicit class ETFromBase[B](b: B) {
    def pureET(implicit  ap: Applicative[Future]): EitherT[Future, Erratum, B] =
      EitherT.pure[Future, Erratum, B](b)
  }

  implicit class ETFApply[B](feb: Future[Either[Erratum, B]]) {
    def wrapET: EitherT[Future, Erratum, B] = EitherT(feb)
  }

  implicit class ETLift[B](eb: Either[Erratum, B]) {
    def liftET(implicit ap: Applicative[Future]): EitherT[Future, Erratum, B] =
      EitherT.fromEither[Future](eb)
  }

  implicit class ETOption[B](ob: Option[B]) {

    def liftET(ifNone: Erratum)(implicit  ap: Applicative[Future]): EitherT[Future, Erratum, B] =
      EitherT.fromOption[Future](ob, ifNone)
  }

  implicit class ETFutOption[B](fob: Future[Option[B]]) {
    def wrapET(ifNone: Erratum)(implicit ap: Applicative[Future],
                                ec: ExecutionContext): EitherT[Future, Erratum, B] =
      fob.map(ob => Either.fromOption(ob, ifNone)).wrapET
  }

  implicit class ETFromFuture[B](fb: Future[B]) {
    def liftET(implicit ec: ExecutionContext): EitherT[Future, Erratum, B] =
      fb.map[Either[Erratum, B]](Right(_))
        .recover { case th: Throwable => Left(Erratum.from(th)) }
        .wrapET
  }

  implicit class ETFromErratum(erratum: Erratum) {
    def pureET[B](implicit ap: Applicative[Future]): EitherT[Future, Erratum, B] =
      Left[Erratum, B](erratum).liftET
  }

  implicit class ETFromEitherThrowable[B](eb: Either[Throwable, B]) {
    def liftET(implicit ap: Applicative[Future]): EitherT[Future, Erratum, B] =
      eb.leftMap(Erratum.from).liftET
  }

  def catchNonFatal[A, B](f: A => B)(a: A)(
      implicit ap: Applicative[Future]): EitherT[Future, Erratum, B] =
    EitherT.fromEither[Future](Either.catchNonFatal(f(a)).leftMap(Erratum.from))

  def cleanErratums[M](seq: Seq[Either[Erratum, M]])(logger: play.api.Logger): Seq[M] = {
    val tup: (Seq[Either[Erratum, M]], Seq[Either[Erratum, M]]) = seq.partition(_.isLeft)
    val errors: Seq[Either[Erratum, M]]                         = tup._1
    val models: Seq[Either[Erratum, M]]                         = tup._2
    errors.foreach(e => logger.error(e.left.get.message))
    models.map(_.right.get)
  }
}
