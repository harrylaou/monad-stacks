package com.harrylaou.monadstacks.spaghetticode

import com.harrylaou.monadstacks.commons.Functions._
import com.harrylaou.monadstacks.commons.User
import com.harrylaou.monadstacks.commons.User._
import play.api.Logger.logger
import play.api.libs.json._
import play.api.mvc.{Action, Controller, Results}

import scala.concurrent.{ExecutionContext, Future}
/**
  * Spaghetti code
  *
  * the  code just :
  * - parse request as json
  * - create user from json
  * - save user in (mongo) db
  * - returns user as json
  *
  */
class UserController(implicit ec: ExecutionContext) extends Controller {

  def saveUser = Action.async { implicit request =>
    val jsonOpt: Option[JsValue] = request.body.asJson
    jsonOpt match {
      case None =>
        logger.error(s" cannot parse json for ${request.body}")
        Future.successful(Results.UnprocessableEntity(" cannot parse json"))
      case Some(jsValue) =>
        val userXor = Json.fromJson(jsValue).asEither
        userXor match {
          case Right(user) =>
            save(user).map { (uOpt: Option[User]) =>
              uOpt match {
                case None =>
                  logger.error(s"couldn't write in db user:$user")
                  InternalServerError(s"couldn't write in db user:$user")
                case Some(u) =>
                  logger.debug(s"User $user updated")
                  Ok(Json.toJson(u))
              }
            }
          case Left(errorData) =>
            logger.error(s"$errorData")
            Future.successful(UnprocessableEntity(s"$errorData"))
        }
    }
  }
}
